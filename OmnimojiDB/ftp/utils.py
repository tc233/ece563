import PIL
import os
import sys
import imghdr
import hashlib


def get_file_md5(path):
    hash_md5 = hashlib.md5()
    blocksize = 1048576  # 1 MBytes
    with open(path, 'rb') as f:
        for chunk in iter(lambda: f.read(blocksize), b''):
            hash_md5.update(chunk)
    md5 = hash_md5.hexdigest()
    return md5

def get_filesize(path):
    return os.path.getsize(path)

def get_file_format(path):
    try:
        fmt = imghdr.what(path)

    except FileNotFoundError:
        print('debug info: get file format file not found!')
        return ''
    if fmt != '' and fmt != None:
        print('file format is', fmt)
        return fmt
    else:
        print('file format is', fmt)
        return 'other'

def generate_url_from_path(path):
    # deprecated, just use file.url
    return ''

def gifsicle_compression(path, compression_parameters):
    if os.path.exists(path):
        os.system('gifsicle ' + compression_parameters + ' -i ' + path + ' -o ' + path)
    return path


def periodicDelete():
    
    return