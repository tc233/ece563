# Generated by Django 2.2 on 2019-04-30 23:03

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ftp', '0002_auto_20190430_1934'),
    ]

    operations = [
        migrations.AlterField(
            model_name='tempdoc',
            name='file_entity',
            field=models.FileField(upload_to=''),
        ),
    ]
