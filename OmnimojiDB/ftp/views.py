from django.shortcuts import render, redirect
from django.views.generic import TemplateView
from django.core.files.storage import FileSystemStorage
from django.contrib.auth.decorators import login_required
from django.contrib.auth import login, authenticate
from django.contrib.auth.forms import UserCreationForm  # for new user
from django.http import HttpResponse, Http404
from .models import Doc, tempDoc  # for api models
from .forms import DocumentForm, SignUpForm, APIForm
from django.views.decorators.csrf import csrf_exempt  # for insecure upload
from django.core.files import File
from django.core.files.base import ContentFile
from django.conf import settings  # for path reference
from django.utils import timezone
from django.core.exceptions import ObjectDoesNotExist

from .utils import *
import datetime
import os.path

# Create your views here.

def Home(request):
    userID = request.user.username
    if userID == 'tc233':
        files = Doc.objects.all()
    else:
        files = Doc.objects.filter(user_id=userID)
        pass
    now_time = timezone.now()
    remaining_time = []
    for f in files:
        remaining_time.append(f.expiry - now_time)
    mylist = zip(files, remaining_time)  # zip to 1 iterable object
    context = {
        'list':mylist,
        # 'files':files,
        # 'nowtime':remaining_time,
    }
    return render(request, 'ftp/home.html', context)


def newUser(request):
    if request.method == 'POST':
        form = SignUpForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(username=username, password=raw_password)
            login(request, user)
            return redirect('home')
    else:
        form = SignUpForm()
    return render(request, 'ftp/newUser.html', {'form': form})


@login_required
def formUpload(request):
    if request.method == 'POST':
        form = DocumentForm(request.POST, request.FILES)
        if form.is_valid():
            doc = Doc()
            doc.file_size = -1
            doc.file_format = 'other'
            doc.view_count = form['view_count'].value()
            doc.file_entity = request.FILES['file_entity']
            doc.file_description = form['file_description'].value()
            doc.path = ''
            doc.time_stamp = datetime.datetime.now()
            doc.expiry = form['expiry'].value()
            doc.user_id = request.user.username
            doc.md5 = ''
            doc.save()
            
            temp_doc = Doc.objects.get(time_stamp=doc.time_stamp)

            doc.file_size = temp_doc.file_entity.size
            path = settings.BASE_DIR + temp_doc.file_entity.url  # the latter has a leading slash
            doc.file_format = get_file_format(path)
            print('debug info: the temp filename is', temp_doc.file_entity.name)
            # path = os.path.join(settings.BASE_DIR, temp_doc.file_entity.url)
            doc.path = path
            # extract the identifier part of the file
            doc.share_url = settings.SHARE_PREFIX + os.path.splitext(temp_doc.file_entity.name)[0]
            doc.file_name = os.path.splitext(temp_doc.file_entity.name)[0]
            doc.md5 = get_file_md5(path)
            # save again to update corresponding fields.
            doc.save()
            return redirect('home')
        else:
            raise Http404("debug info: gui form upload is not valid")
    else:
        form = DocumentForm()
    return render(request, 'ftp/formUpload.html', {
        'form': form
    })

# when a file with same filename is uploaded twice, 
# check their md5 value to see if the same file

# after save the file

@csrf_exempt
def upload_api(request):
    if request.method == 'POST':
        form = APIForm(request.POST, request.FILES)
        if form.is_valid():

            temp_doc = tempDoc()
            temp_doc.file_entity = request.FILES['file_entity']
            temp_doc.save()

            temp_path = settings.BASE_DIR + temp_doc.file_entity.url
            temp_md5 = get_file_md5(temp_path)
            if temp_md5 != form['md5'].value():
                print('original md5 = ', form['md5'].value())
                print('uploaded md5 = ', temp_md5)
                return HttpResponse("md5 failed")
            else:
                print('md5 comparison succeeded')
                print('original file size is: ', temp_doc.file_entity.size)

            doc = Doc()
            gifsicle_compression(temp_path, form['compression_parameters'].value())

            with open(temp_path, 'rb') as outfile:
                # doc.file_entity.save(temp_doc.file_entity.name, File(outfile))
                # doc.file_entity = ContentFile(outfile.read())
                doc.file_entity.save(temp_doc.file_entity.name, ContentFile(outfile.read()))

            
            temp_doc = tempDoc.objects.get(pk=temp_doc.file_id)

            path = settings.BASE_DIR + doc.file_entity.url

            doc.file_size = temp_doc.file_entity.size
            doc.file_format = get_file_format(path)
            print('debug info: the temp filename is', temp_doc.file_entity.name)
            print('debug info: the real filename is', doc.file_entity.name)
            # path = os.path.join(settings.BASE_DIR, temp_doc.file_entity.url)
            doc.path = path
            doc.md5 = get_file_md5(path)
            # save again to update corresponding fields.
            doc.share_url = settings.SHARE_PREFIX + os.path.splitext(doc.file_entity.name)[0]
            print('share', doc.share_url)
            doc.file_name = os.path.splitext(doc.file_entity.name)[0]
            # doc.file_size = -1
            # doc.file_format = ''
            doc.view_count = form['view_count'].value()
            # doc.file_entity = request.FILES['file_entity']
            doc.file_description = form['file_description'].value()
            # doc.path = ''
            doc.time_stamp = datetime.datetime.now()
            doc.expiry = form['expiry'].value()
            doc.user_id = form['user_id'].value()
            doc.md5 = form['md5'].value()
            doc.save()
            # temp Doc is not used anymore, just delete it
            temp_doc.delete()
            return HttpResponse(doc.share_url)
        else:
            return HttpResponse("failed")  # used for swift checking
    else:
        return HttpResponse("not post method")


def share(request, filename):
    # check if is an image, if not, just return the file url itself
    try:
        temp_doc = Doc.objects.get(file_name=filename)
    except ObjectDoesNotExist:
        return HttpResponse("<h1>Image does not exist. Might be erased due to expiration.</h1>")


    if temp_doc.view_count >= 1:
        temp_doc.view_count -= 1;
        temp_doc.save()

        if temp_doc.file_format == 'other':  # this is a binary file
            retfile = File(open(temp_doc.path, "rb"))
            response = HttpResponse(retfile, content_type='application/octet-stream')
            response['Content-Disposition'] = 'attachment; filename=%s' % temp_doc.file_name
            response['Content-Length'] = retfile.size
            return response
        else:  # this is an image file
            context = {
                'doc': temp_doc,
            }
            return render(request, 'ftp/share.html', context) 
    else: 
        return HttpResponse("<h1>Image exceeded maximum view count.</h1>")
    

# instead of doing thumbnails for all the files, just list them all so that no need to add another field.
def listall(request, userID):
    temp_doc = Doc.objects.filter(user_id=userID)
    context = {
        'docs': temp_doc,
    }
    return render(request, 'ftp/thumbList.html', context)

