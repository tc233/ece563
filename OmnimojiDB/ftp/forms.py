from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User

from .models import Doc


class DocumentForm(forms.ModelForm):
    class Meta:
        model = Doc
        fields = ['file_description', 'view_count', 'file_entity', 'expiry']



class SignUpForm(UserCreationForm):
    first_name = forms.CharField(max_length=30, required=False, help_text='Optional.')
    last_name = forms.CharField(max_length=30, required=False, help_text='Optional.')
    email = forms.EmailField(max_length=254, help_text='Required. Inform a valid email address.')

    class Meta:
        model = User
        fields = ['username', 'first_name', 'last_name', 'email', 'password1', 'password2', ]

class APIFormBase(forms.ModelForm):
	class Meta:
		model = Doc
		fields = ('expiry', 'view_count', 'md5', 'user_id', 'file_entity', 'file_description')

class APIForm(APIFormBase):

    compression_parameters = forms.CharField(max_length=255, required=False)

    class Meta(APIFormBase.Meta):
        fields = APIFormBase.Meta.fields + ('compression_parameters',)
