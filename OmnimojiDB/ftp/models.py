from django.db import models
from django.utils.timezone import now
from django.db.models.signals import post_delete
from django.dispatch import receiver

# Create your models here.

class Doc(models.Model):
    file_id = models.AutoField(primary_key=True)
    file_size = models.IntegerField(default=-1)  # fill in by backend
    file_format = models.CharField(max_length=5, default='other')  # fill in by backend
    view_count = models.IntegerField(default=1)  # input by user
    # file will be uploaded to MEDIA_ROOT/
    file_entity = models.FileField(upload_to='')
    file_description = models.TextField()
    path = models.CharField(max_length=255, blank=True)  # fill in by backend
    time_stamp = models.DateTimeField(default=now, editable=False)  # fill in by backend
    expiry = models.DateTimeField(default=now, editable=True)   # input by user or 
    user_id = models.CharField(max_length=36, blank=True)  # fill in by backend; if webpage, username
    md5 = models.CharField(max_length=32, blank=True)  # to check if update is the same. same do not update
    share_url = models.CharField(max_length=255, blank=True)  # fill in by backend
    file_name = models.CharField(max_length=255, blank=True)  # name without extension and path, self.file_entity.name split [0]
    def __str__(self):
        return self.file_entity.name

    class Meta:
        db_table = 'Docs'


# @receiver(post_delete, sender=Doc)
# def submission_delete(sender, instance, **kwargs):
#     instance.file_entity.delete(False)

class tempDoc(models.Model):
    # file will also be uploaded to MEDIA_ROOT/ , and override under that folder
    file_id = models.AutoField(primary_key=True)
    file_entity = models.FileField(upload_to='')

    class Meta:
        db_table = 'tempDocs'

    def __str__(self):
        return self.file_entity.name

# https://matthiasomisore.com/uncategorized/django-delete-file-when-object-is-deleted/
# delete file when object deleted


# @receiver(post_delete, sender=tempDoc)
# def submission_delete(sender, instance, **kwargs):
#     instance.file_entity.delete(False) 
