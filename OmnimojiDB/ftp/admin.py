from django.contrib import admin

# Register your models here.
from .models import Doc, tempDoc

admin.site.register(Doc)
admin.site.register(tempDoc)
