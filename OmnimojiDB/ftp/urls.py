from django.urls import path
from django.conf.urls import include
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static
from . import views

urlpatterns = [
    path('', views.Home, name='home'),
    # path('upload/', views.upload, name='upload'),
    path('newUser/', views.newUser, name='newUser'),
    path('formUpload/', views.formUpload, name='formUpload'),
    path('APIUpload/', views.upload_api, name='upload_api'),
    path('admin/', admin.site.urls),
    path('accounts/', include('django.contrib.auth.urls')),
    path('share/<str:filename>', views.share, name='share'),
    path('listall/<str:userID>', views.listall, name='listall'),
]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
