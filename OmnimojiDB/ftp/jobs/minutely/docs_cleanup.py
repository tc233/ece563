# -*- coding: utf-8 -*-
"""
Daily cleanup job.
Can be run as a cronjob to clean out old data from the database (only expired
sessions at the moment).
https://django-extensions.readthedocs.io/en/latest/jobs_scheduling.html
"""
import datetime
from django.utils import timezone

from ...models import Doc

from django_extensions.management.jobs import MinutelyJob


class Job(MinutelyJob):
    help = "doc table entries cleanup Job"

    def execute(self):
        docs = Doc.objects.all()
        # now_time = datetime.datetime.now()
        now_time = timezone.now()
        for doc in docs:
        	if doc.expiry < now_time:
        		print(now_time)
        		print(doc.expiry)
        		print('doc is too old, delete.', doc.file_id)
        		doc.delete()


